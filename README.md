# Test command

Below command test if `nvenc` is working as expected

```
ffmpeg -f lavfi -i nullsrc=s=1280x720:d=10 -filter_complex "geq=random(1)*255:128:128" -c:v h264_nvenc -y output.mp4
```
